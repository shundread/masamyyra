#!/bin/sh

. "$(dirname $0)/common.sh"

# Set path for node binaries
PATH=${PATH}:./node_modules/.bin

# Important paths
buildFolder="build/graphics"
sourceFolder="data/graphics"
workFolder="work/graphics"
spritesheetFile="./src/spritesheet.ts"

echo
echo
echo "${yellow}Bundling graphics${noColor}"
echo
echo

# Clear graphics build folder
if [ ! -d "build" ]; then
  mkdir --verbose "build"
fi
rm -rf --verbose "${buildFolder}"
mkdir --verbose "${buildFolder}"

# Clear font work folder
if [ ! -d "work" ]; then
  mkdir --verbose "work"
fi
rm -rf --verbose "${workFolder}"
mkdir --verbose "${workFolder}"

# Graphics packaging
bundleSpritesheet()
{
  folder="${1}"
  printf "Bundling folder %s..." "${workFolder}/${folder}"
  spritesheet-js --format=pixi.js --name="${folder}" --path="${buildFolder}" --padding=2 ./"${workFolder}"/"${folder}"/*.png
}

# Spritesheet constant type annotation and declaration
generateSpritesheetConstant()
{
  spritesheet=${1}
  printf "Generating typescript spritesheet definition for ${spritesheet}..."
  mkdir --verbose "${workFolder}/${folder}"

  files=$(cd "${sourceFolder}"/"${spritesheet}" && ls ./*.png)

  outputFile="${workFolder}/${folder}.ts_chunk"
  {
    echo ""
    echo "export const ${spritesheet}: {"
  } >> "${outputFile}"

  # Field types
  for file in ${files}
  do
    trimmed=$(echo "${file}" | sed "s/.png//" | sed "s!./!!")
    echo "  ${trimmed}: undefined | PIXI.Texture;" >> "${outputFile}"
    cp --verbose "${sourceFolder}/${folder}/${trimmed}.png" "${workFolder}/${folder}"
  done

  # Delimiter between type definition & value initialization
  echo "} = {" >> "${outputFile}"

  # Field values
  for file in ${files}
  do
    trimmed=$(echo "${file}" | sed "s/.png//" | sed "s!./!!")
    echo "  ${trimmed}: undefined," >> "${outputFile}"
  done

  echo "};" >> "${outputFile}"
  echo "Done"
}

folders=$(ls "${sourceFolder}")
for folder in ${folders}
do
  generateSpritesheetConstant "${folder}"
  bundleSpritesheet "${folder}"
done

# Combining spritesheets onto a single file
rm -f --verbose "${spritesheetFile}"
{
  echo "// Auto-generated file, do not edit directly but run make-graphics.sh instead"
  echo ""
  echo "// External libraries"
  echo 'import * as PIXI from "pixi.js";'
  echo ""
  cat "${workFolder}"/*.ts_chunk
} >> "${spritesheetFile}"
echo "Generated ${spritesheetFile}"
