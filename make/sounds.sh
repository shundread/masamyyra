#!/bin/sh

. "$(dirname $0)/common.sh"

# Important paths
buildFolder="build"
sourceFolder="data/sounds"

echo
echo
echo "${yellow}Bundling sounds${noColor}"
echo
echo

# Clear fonts build folder
if [ ! -d "build" ]; then
  mkdir --verbose "build"
fi
cp --verbose -rf "${sourceFolder}" "${buildFolder}"
