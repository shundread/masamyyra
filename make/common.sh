#!/bin/sh

set -eu

cd "$(dirname $0)/.."

export workFolder="./work"
export buildFolder="./build"

# Colors
export noColor='\033[0m'
export lightGreen='\033[1;32m'
export yellow='\033[1;33m'
export lightPurple='\033[1;35m'
