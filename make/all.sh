#!/bin/sh

. "$(dirname $0)/common.sh"

# Clear whole work folder
rm -rf --verbose "work"
rm --verbose -rf "build"

# Resolve build type:
# accept only 'production' as argument, or default to 'development' build
if [ "${#}" = "1" ]; then
  if [ "$1" = "production" ]; then
    build="production"
  else
    echo "The only accepted arguments for the make script are none or 'production'"
    exit 1
  fi
else
  echo "Making a development build. Run '$0 production' if you wish to make a production build"
  build="development"
fi

# Announce build type
if [ "${build}" = "production" ]; then
  buildEcho="${lightGreen}(PRODUCTION BUILD)${noColor}"
else
  buildEcho="${yellow}(DEVELOPMENT BUILD)${noColor}"
fi
echo
echo
echo "Building ${lightPurple}Masa Myyrä ${buildEcho}"
echo
echo

# Ship static files
cp --verbose --recursive --force "static" "build"

rm --verbose --recursive --force "build/licenses"
rm --verbose --recursive --force "build/lib"
mkdir --verbose --parents "build/licenses"
mkdir --verbose --parents "build/lib"

# Ship auxiliary libraries / packages
cp --verbose "node_modules/mobile-detect/mobile-detect.js" "build/lib"
cp --verbose "node_modules/mobile-detect/LICENSE" "build/licenses/mobile-detect.txt"

cp --verbose "node_modules/pixi.js/dist/pixi.js" "build/lib"
cp --verbose "node_modules/pixi.js/dist/pixi.js.map" "build/lib"
cp --verbose "node_modules/pixi.js/LICENSE" "build/licenses/pixijs.txt"

cp --verbose "node_modules/pixi-sound/dist/pixi-sound.js" "build/lib"
cp --verbose "node_modules/pixi-sound/dist/pixi-sound.js.map" "build/lib"
cp --verbose "node_modules/pixi-sound/LICENSE" "build/licenses/pixi-sound.txt"

cp --verbose "node_modules/tween.js/src/Tween.js" "build/lib/tween.js"
cp --verbose "node_modules/tween.js/LICENSE" "build/licenses/tweenjs.txt"

# Bundle fonts, graphics, sounds
./make/fonts.sh
./make/graphics.sh
./make/sounds.sh

# Compilation TODO split to make-compile.sh
echo "Compiling..."
if [ "${build}" = "production" ]; then
  ./make/compile.sh "production"
else
  ./make/compile.sh
fi
