// External libraries
import * as PIXI from "pixi.js";
import * as TWEEN from "tween.js";

// Generated libraries
import * as Spritesheet from "./spritesheet";

// Game libraries
import * as Duration from "./duration";

// Use this on boot to measure the scene transition delay once loading completes
const ProgressDuration: number = Duration.Short;
const BounceDuration: number = Duration.Long;

//
// Loading screen
//

class Loader {
  public stage: PIXI.Container = new PIXI.Container();

  private notLoaded: PIXI.Sprite = new PIXI.Sprite(Spritesheet.loading.notLoaded);
  private loaded: PIXI.Sprite = new PIXI.Sprite(Spritesheet.loading.loaded);
  private progressMask: PIXI.Graphics = new PIXI.Graphics();

  private tween: null | TWEEN.Tween = null;

  public constructor(startGame: () => void) {
    this.notLoaded.anchor.set(0.5, 0.5);
    this.stage.addChild(this.notLoaded);

    this.loaded.anchor.set(0.5, 0.5);
    this.stage.addChild(this.loaded);

    this.progressMask.isMask = true;
    this.progressMask.beginFill(0xffffff, 1);
    this.progressMask.drawRect(0, 0, this.loaded.width, this.loaded.height);
    this.progressMask.endFill();
    this.loaded.mask = this.progressMask;

    // We set the mask parent to the masked object to make it easier to position
    // relative to the masked object. Pivot y-coordinate is -1x loaded height
    // so that y=0 in its coordinates place it one full height above the masked
    // image + 0.5x the height to account for the vertical centering of the
    // image.
    // As the mask starts overlapping the masked object it reveals the object
    // to the player
    this.loaded.addChild(this.progressMask);
    this.progressMask.pivot.set(0.5 * this.loaded.width, 1.5 * this.loaded.height);

    // Set interactivity response and bounds (but don't yet activate it)
    this.stage.on("pointerup", startGame);
  }

  public setProgress(fraction: number) {
    if (this.tween) {
      TWEEN.remove(this.tween);
    }
    this.tween = new TWEEN.Tween(this.progressMask.position)
      .to({ y: fraction * this.loaded.height }, ProgressDuration)
      .easing(TWEEN.Easing.Quadratic.Out)
      .start();
  }

  public setReady() {
    if (this.tween) {
      TWEEN.remove(this.tween);
    }

    this.stage.removeChild(this.notLoaded);
    this.notLoaded.destroy();

    // Somehow indicate that the game is ready
    this.tween = new TWEEN.Tween(this.loaded.scale)
      .to({ x: 1.3, y: 1.3 }, BounceDuration)
      .easing(TWEEN.Easing.Sinusoidal.InOut)
      .yoyo(true)
      .repeat(Infinity)
      .start();

    this.stage.interactive = true;
  }

  public destroy() {
    if (this.tween) {
      TWEEN.remove(this.tween);
    }

    for (const displayObject of this.stage.removeChildren()) {
      displayObject.destroy();
    }

    this.stage.parent.removeChild(this.stage);
    this.stage.destroy();
  }
}

export {
  Loader,
}
