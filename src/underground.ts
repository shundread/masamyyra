// External libraries
import * as PIXI from "pixi.js";

// Game libraries
import * as DisplayInfo from "./displayInfo";
import { Masa } from "./masa";
import * as Spritesheet from "./spritesheet";

const DigPointRadius: number = 100;
const DiamondsQuantity: number = 3;
const MaxTimer: number = 1000 * 10;

class Underground {
  public stage: PIXI.Container = new PIXI.Container();
  public ySpeed: number = 0;
  public timer: number = MaxTimer;
  public height: number;
  public playing: boolean = false;

  private undugLayer: PIXI.extras.TilingSprite;
  private dugLayer: PIXI.extras.TilingSprite;
  private dugMask: PIXI.Graphics = new PIXI.Graphics();
  private dugPath: PIXI.Point[] = [];

  private diamonds: PIXI.Sprite[] = [];

  private moveToColumnCallback: (column: number) => void;
  private gameOverCallback: () => void;
  private collectDiamondCallback: (diamond: PIXI.Sprite) => void;
  private updateTimerCallback: (value: string) => void;

  constructor(
    moveToColumnCallback: (column: number) => void,
    gameOverCallback: () => void,
    collectDiamondCallback: (diamond: PIXI.Sprite) => void,
    updateTimerCallback: (value: string) => void,
  ) {
    this.height = DisplayInfo.Height;
    this.moveToColumnCallback = moveToColumnCallback;
    this.gameOverCallback = gameOverCallback;
    this.collectDiamondCallback = collectDiamondCallback;
    this.updateTimerCallback = updateTimerCallback;

    // Spawn tiling sprites
    this.undugLayer = new PIXI.extras.TilingSprite(
      Spritesheet.terrain.undug as PIXI.Texture,
      DisplayInfo.Width,
      DisplayInfo.Height);
    this.dugLayer = new PIXI.extras.TilingSprite(
      Spritesheet.terrain.dug as PIXI.Texture,
      DisplayInfo.Width,
      DisplayInfo.Height);

    // Set object anchors
    this.undugLayer.anchor.set(0.5, 0.5);
    this.dugLayer.anchor.set(0.5, 0.5);

    // Set object hierarchy
    this.stage.addChild(this.undugLayer);
    this.stage.addChild(this.dugLayer);
    this.stage.addChild(this.dugMask);
    this.dugLayer.mask = this.dugMask;

    // Set interactivity
    this.stage.on("pointerdown", (pointerEvent: PIXI.interaction.InteractionEvent) => this.handleTap(pointerEvent));
    this.stage.on("pointermove", (pointerEvent: PIXI.interaction.InteractionEvent) => this.handleTap(pointerEvent));
  }

  public update(timeMs: number, masa: Masa) {
    const shift = this.ySpeed * timeMs;
    this.height += shift;

    // Start digging
    if (this.height < 0) {
      this.dugPath.push(new PIXI.Point(masa.body.position.x, masa.body.position.y + 125));
    }

    if (this.height > 0) {
      this.stage.position.y = this.height;
    } else {
      this.stage.position.y = 0;

      if (this.timer > 0) {
        this.timer = Math.max(0, this.timer - timeMs);
        this.updateTimerCallback(`${Math.floor(this.timer / 1000)}`);
      } else {
        if (this.playing === true) {
          this.playing = false;
          this.setInteractive(false);
          this.gameOverCallback();
        }
      }
    }

    let tileHeight: number;
    if (this.height > 0) {
      tileHeight = 0;
    } else {
      tileHeight = DisplayInfo.Height + this.height;
      if (this.diamonds.length === 0) {
        this.spawnDiamonds();
      }
    }

    // Shift layers
    this.undugLayer.tilePosition.y = tileHeight;

    // Shift the dug path and filter out points that are definitely no longer
    // relevant
    for (const point of this.dugPath) {
      point.y += shift;
    }
    this.dugPath = this.dugPath.filter((p: PIXI.Point) => p.y >= -DisplayInfo.Height);

    // Redraw dug path
    this.dugMask.clear();
    for (const point of this.dugPath) {
      this.dugMask.beginFill(0x000000);
      this.dugMask.drawCircle(point.x, point.y, DigPointRadius);
      this.dugMask.endFill();
    }

    // Move diamonds
    let remaining: PIXI.Sprite[] = [];

    for (const diamond of this.diamonds) {
      diamond.position.y += shift;

      // Remove missed diamonds
      if (diamond.position.y < -0.5 *  this.undugLayer.height) {
        diamond.visible = false;
        this.stage.removeChild(diamond);
      } else {
        remaining.push(diamond);
      }
    }

    // Collect diamonds
    for (const diamond of this.diamonds) {
      const dx = Math.abs(masa.body.position.x - diamond.position.x);
      const dy = Math.abs(masa.body.position.y - diamond.position.y);
      if (dx < 100 && dy < 300) {
        this.timer += 1800;
        this.collectDiamondCallback(diamond);
        remaining = remaining.filter((d: PIXI.Sprite) => d !== diamond);
      }
    }

    // Filter collected & missed diamonds
    this.diamonds = remaining;
  }

  public setInteractive(value: boolean) {
    if (value === true) {
      this.timer = MaxTimer;
    }
    this.playing = value;
    this.stage.interactive = value;
  }

  private handleTap(pointerEvent: PIXI.interaction.InteractionEvent) {
    if (this.playing === false) {
      return;
    }

    // Find the local coordinat
    const local: PIXI.Point = this.undugLayer.toLocal(pointerEvent.data.global);

    // Shift to compensate the anchor
    local.x += 0.5 * this.undugLayer.width;

    // Find the column
    const column: number = Math.floor(local.x / (Spritesheet.terrain.dug as PIXI.Texture).width) - 2;
    const clamped: number = Math.min(2, Math.max(-2, column));
    return this.moveToColumnCallback(clamped);
  }

  private spawnDiamonds() {
    while (this.diamonds.length < DiamondsQuantity) {
      let texture: PIXI.Texture;
      if (Math.random() < 0.33) {
        texture = Spritesheet.diamond.a as PIXI.Texture;
      } else if (Math.random() < 0.33) {
        texture = Spritesheet.diamond.b as PIXI.Texture;
      } else {
        texture = Spritesheet.diamond.c as PIXI.Texture;
      }

      const diamond: PIXI.Sprite = new PIXI.Sprite(texture);
      diamond.anchor.set(0.5, 0.5);
      const column: number = Math.floor(Math.random() * 5);
      const x: number = (column - 2) * (Spritesheet.terrain.dug as PIXI.Texture).width;
      const y: number = (1 + 3 * Math.random()) * this.undugLayer.height;
      diamond.position.set(x, y);
      this.diamonds.push(diamond);

      this.stage.addChild(diamond);
    }
  }
}

export {
  Underground,
  MaxTimer,
};
