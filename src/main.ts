// External libraries
import * as TWEEN from "tween.js";

// Game libraries
import * as Audio from "./audio";
import * as DisplayInfo from "./displayInfo";
import * as Duration from "./duration";
import * as FontNames from "./fontNames";
import * as Image from "./image";
import { Loader } from "./loader";
import { Masa } from "./masa";
import * as Spritesheet from "./spritesheet";
import { SurfaceScene } from "./surfaceScene";
import * as UI from "./ui";
import { MaxTimer, Underground } from "./underground";
import { wait, waitTween } from "./waiter";

const Credits: string[] = [
  "Kaarle 7-v.",
  "Mikko Aaltio",
  "Thiago Chaves\nde Oliveira\nHorta",
  "Jessica Lillbåsk",
];
const SkipCredits: boolean = false

// Text style
const ScoreTextStyle: PIXI.extras.BitmapTextStyle = {
  align: "center",
  font: FontNames.komikagl,
  tint: 0x6dcff6,
};

const TimerTextStyle: PIXI.extras.BitmapTextStyle = {
  align: "center",
  font: FontNames.komikagl,
  tint: 0xffffff,
};

let diamondsLayer: PIXI.Container;
let surfaceScene: SurfaceScene;
let underground: Underground;
let masa: Masa;
let targetPosition: number;

// Hud elements
let scoreIndicator: PIXI.Sprite;
let scoreText: PIXI.extras.BitmapText;
let timerIndicator: PIXI.Sprite;
let timerText: PIXI.extras.BitmapText;
let totalScoreText: PIXI.extras.BitmapText;

let score: number = 0;
let totalScore: number = 0;

function boot() {
  console.log("Booting our application");
  UI.createRenderer();
  PIXI.ticker.shared.add(tick);
  loadGameData();
}

let lastTime: number = Date.now();
function tick() {
  TWEEN.update();

  // Find current time & dt
  const time: number = Date.now();
  const dt: number = time - lastTime;

  if (surfaceScene && surfaceScene.stage.visible) { surfaceScene.update(dt); }
  if (underground && masa && underground.stage.visible) { underground.update(dt, masa); }
  if (underground) {
    surfaceScene.stage.position.y = underground.stage.position.y - DisplayInfo.Height;
  }

  lastTime = time;
}

let loader: Loader;

function loadGameData() {
  // Load the minimum set of assets for the loader
  PIXI.loader.add("loading", "./graphics/loading.json");
  PIXI.loader.load(showViewLoader);
}

function showViewLoader() {
  // Initialize the loader information
  Image.initializeSpritesheet("loading");

  // Show the loader
  loader = new Loader(playIntro);
  UI.stage.addChild(loader.stage);

  // Load remaining resources
  Image.loadRemainingSpritesheets();
  Audio.loadSounds();
  PIXI.loader.add("fontKomika", "./fonts/komikagl.xml");

  // Set callbacks for resource loading progress and completion
  PIXI.loader.onProgress.add(onLoadProgress);
  PIXI.loader.load(allowLoaderDismiss);
}

async function onLoadProgress(arg: any) {
  loader.setProgress(0.01 * arg.progress);
}

async function allowLoaderDismiss() {
  // Initialize all assets
  await Audio.initializeSounds();
  await Image.initializeRemainingSpritesheets();

  // Set loader to ready to start game
  loader.setReady();
}

async function playIntro() {
  // Remove the loader, we'll never need it again at this point
  loader.destroy();

  surfaceScene = new SurfaceScene(startGame);
  await revealsurfaceScene();
}

async function revealsurfaceScene() {
  diamondsLayer = new PIXI.Container();
  UI.stage.addChild(surfaceScene.stage);
  UI.stage.addChild(diamondsLayer);
  surfaceScene.stage.alpha = 0;
  await waitTween(new TWEEN.Tween(surfaceScene.stage)
    .to({ alpha: 1 }, Duration.FadeIn)
    .easing(TWEEN.Easing.Quadratic.Out));

  await wait(500);

  Audio.startMusic();
  await waitTween(new TWEEN.Tween(surfaceScene)
    .to({ ySpeed: -2 }, Duration.StartDrop)
    .easing(TWEEN.Easing.Quadratic.In));

  // Show credits
  if (SkipCredits === false) {
    for (const name of Credits) {
      await surfaceScene.showText(name);
    }
    await surfaceScene.showText("Masa Myyrä\nja\nHerra Hakku", 2.0, 1.5);
  }

  // Reval house
  await surfaceScene.addHouse(Duration.HouseArrive);

  // Spawn score indicator
  scoreIndicator = new PIXI.Sprite(Spritesheet.diamond.collection);
  scoreIndicator.position.set(-0.4 * DisplayInfo.Width, -0.4 * DisplayInfo.Height);
  scoreIndicator.anchor.set(0.5, 0.5);
  scoreIndicator.visible = false;
  scoreText = new PIXI.extras.BitmapText("0", ScoreTextStyle);
  scoreText.position.set(-0.4 * DisplayInfo.Width, -0.4 * DisplayInfo.Height + 80);
  scoreText.anchor = new PIXI.Point(0.5, 0.5);
  scoreText.visible = false;

  // Spawn timer indicator
  timerIndicator = new PIXI.Sprite(Spritesheet.diamond.clock);
  timerIndicator.position.set(0.4 * DisplayInfo.Width, -0.4 * DisplayInfo.Height);
  timerIndicator.anchor.set(0.5, 0.5);
  timerIndicator.visible = false;
  timerText = new PIXI.extras.BitmapText(`${MaxTimer / 1000}`, TimerTextStyle);
  timerText.position.set(0.4 * DisplayInfo.Width, -0.4 * DisplayInfo.Height + 80);
  timerText.anchor = new PIXI.Point(0.5, 0.5);
  timerText.visible = false;

  // Total score indicator
  const scoreStr: string | null = window.localStorage.getItem("totalScore");
  if (scoreStr) {
    totalScore = Number(scoreStr);
  } else {
    totalScore = 0;
  }

  totalScoreText = new PIXI.extras.BitmapText(`${totalScore}`, ScoreTextStyle);
  totalScoreText.position.set(0.1 * DisplayInfo.Width, -0.05 * DisplayInfo.Height + 15);
  totalScoreText.anchor = new PIXI.Point(0.5, 0.5);

  surfaceScene.stage.addChild(totalScoreText);

  // Spawn masa offscreen
  masa = new Masa();
  masa.body.position.set(-2 * DisplayInfo.Width, 200);

  // Spawn underground scene
  underground = new Underground(
    moveToColumn,
    gameOver,
    collectDiamond,
    updateTimer);

  // Set object hierarchy
  UI.stage.addChild(underground.stage);
  UI.stage.addChild(masa.body);
  UI.stage.addChild(scoreIndicator);
  UI.stage.addChild(scoreText);
  UI.stage.addChild(timerIndicator);
  UI.stage.addChild(timerText);

  // Set underground position
  underground.stage.position.set(0, DisplayInfo.Height);

  // Reveal masa
  new TWEEN.Tween(masa.body.position)
    .to({ x: -200 }, Duration.MasaReveal)
    .easing(TWEEN.Easing.Quadratic.Out)
    .start();

  surfaceScene.setInteractive(true);
}

async function startGame() {
  surfaceScene.setInteractive(false);

  scoreIndicator.visible = true;
  scoreText.visible = true;
  timerIndicator.visible = true;
  timerText.visible = true;
  //
  // Hops up
  //
  new TWEEN.Tween(masa.body.position)
    .to({ x: -100, y: -400}, Duration.HalfFlip)
    .easing(TWEEN.Easing.Sinusoidal.Out)
    .start();
  new TWEEN.Tween(masa.body)
    .to({ rotation: Math.PI / 2 }, Duration.HalfFlip)
    .easing(TWEEN.Easing.Sinusoidal.Out)
    .start();
  masa.setArmDiving();
  await wait(Duration.HalfFlip);

  //
  // Start drop down
  //
  new TWEEN.Tween(masa.body.position)
    .to({ x: 0 }, Duration.HalfFlip)
    .easing(TWEEN.Easing.Sinusoidal.In)
    .start();
  new TWEEN.Tween(masa.body)
    .to({ rotation: Math.PI }, Duration.HalfFlip)
    .easing(TWEEN.Easing.Sinusoidal.In)
    .start();

  //
  // Start moving the underround
  //
  new TWEEN.Tween(underground)
    .to({ ySpeed: -0.75 }, Duration.HalfFlip)
    .easing(TWEEN.Easing.Sinusoidal.In)
    .start();
  await wait(Duration.HalfFlip);

  //
  // Start digging upon touching the underground
  //
  masa.setArmWaving();
  targetPosition = Math.PI;
  underground.setInteractive(true);
}

function moveToColumn(index: number) {
  const position: number = index * (Spritesheet.terrain.undug as PIXI.Texture).width;
  if (position !== targetPosition) {
    new TWEEN.Tween(masa.body.position)
      .to({ x: position }, Duration.MoveToColumn)
      .start();
  }
  targetPosition = position;
}

async function gameOver() {
  // Stop digging and slow down
  masa.setArmResting();
  await waitTween(new TWEEN.Tween(underground)
    .to({ ySpeed: 0 }, Duration.SlowDown)
    .easing(TWEEN.Easing.Sinusoidal.InOut));

  // Quickly slide back to surface
  await waitTween(new TWEEN.Tween(underground)
    .to({ height: DisplayInfo.Height }, Duration.Huge)
    .easing(TWEEN.Easing.Sinusoidal.InOut));

  //
  // Jump back to surface and unflip Masa
  //
  new TWEEN.Tween(masa.body.position)
    .to({ x: -200, y: 200 }, Duration.HalfFlip)
    .easing(TWEEN.Easing.Quadratic.Out)
    .start();
  new TWEEN.Tween(masa.body)
    .to({ rotation: 0 }, Duration.HalfFlip)
    .easing(TWEEN.Easing.Quadratic.Out)
    .start();
  await wait(Duration.HalfFlip);

  //
  // Dump all diamonds into the house
  //
  const diamonds: PIXI.Sprite[] = [];
  const hadDiamonds: boolean = score > 0;
  while (score > 0) {
    //
    // Deduct score from latest play add to global score
    //
    score--;
    totalScore++;
    scoreText.text = `${score}`;
    totalScoreText.text = `${totalScore}`;

    //
    // Spawn each diamond as a random texture
    //
    let texture: PIXI.Texture;
    if (Math.random() < 0.33) {
      texture = Spritesheet.diamond.a as PIXI.Texture;
    } else if (Math.random() < 0.33) {
      texture = Spritesheet.diamond.b as PIXI.Texture;
    } else {
      texture = Spritesheet.diamond.c as PIXI.Texture;
    }
    const diamond: PIXI.Sprite = new PIXI.Sprite(texture);
    diamond.anchor.set(0.5, 0.5);
    diamondsLayer.addChild(diamond);
    diamonds.push(diamond);

    //
    // Send each diamond to the house's door while shrinking down into oblivion
    //
    Audio.diamond.play();
    diamond.position.set(scoreIndicator.position.x, scoreIndicator.position.y);
    new TWEEN.Tween(diamond.scale)
      .to({ x: 0, y: 0 }, Duration.DumpDiamond)
      .easing(TWEEN.Easing.Quadratic.In)
      .start();
    new TWEEN.Tween(diamond.position)
      .to({ x: 200, y: 300 }, Duration.DumpDiamond)
      .easing(TWEEN.Easing.Quadratic.In)
      .start();
    await wait(Duration.DumpDiamondInterval);
  }

  // Wait until last diamond finishes dumping (only if we had any)
  if (hadDiamonds) {
    await wait(Duration.DumpDiamond - Duration.DumpDiamondInterval);
  }

  // Clear any diamond sprites we may have created
  for (const diamond of diamonds) {
    diamondsLayer.removeChild(diamond);
    diamond.destroy();
  }

  // Save the total score
  window.localStorage.setItem("totalScore", `${totalScore}`);

  // Allow player to start a new game
  surfaceScene.setInteractive(true);
}

async function collectDiamond(diamond: PIXI.Sprite) {
  //
  // Slide diamond towards score UI element while shrinking
  //
  Audio.diamond.play();
  new TWEEN.Tween(diamond.scale)
    .to({ x: 0.25, y: 0.25 }, Duration.CollectDiamond)
    .easing(TWEEN.Easing.Quadratic.In)
    .start();
  await waitTween(new TWEEN.Tween(diamond.position)
    .to({ x: scoreIndicator.position.x,
          y: scoreIndicator.position.y }, Duration.CollectDiamond)
    .easing(TWEEN.Easing.Quadratic.In));
  diamond.parent.removeChild(diamond);
  diamond.destroy();

  // Once the diamond sprite arrives, bump up score and update score indicator
  // and play a bump up animation.
  score += 1;
  scoreText.text = `${score}`;
  scoreText.scale.set(1.5, 1.5);
  new TWEEN.Tween(scoreText.scale)
    .to({ x: 1, y: 1}, 200)
    .start();
  scoreIndicator.scale.set(1.5, 1.5);
  new TWEEN.Tween(scoreIndicator.scale)
    .to({ x: 1, y: 1}, 200)
    .start();
}

function updateTimer(text: string) {
  timerText.text = text;
}

document.addEventListener("DOMContentLoaded", boot);
