// Auto-generated file, do not edit directly but run make-graphics.sh instead

// External libraries
import * as PIXI from "pixi.js";


export const diamond: {
  a: undefined | PIXI.Texture;
  b: undefined | PIXI.Texture;
  clock: undefined | PIXI.Texture;
  collection: undefined | PIXI.Texture;
  c: undefined | PIXI.Texture;
} = {
  a: undefined,
  b: undefined,
  clock: undefined,
  collection: undefined,
  c: undefined,
};

export const intro: {
  cloud: undefined | PIXI.Texture;
  houseScene: undefined | PIXI.Texture;
} = {
  cloud: undefined,
  houseScene: undefined,
};

export const loading: {
  loaded: undefined | PIXI.Texture;
  notLoaded: undefined | PIXI.Texture;
} = {
  loaded: undefined,
  notLoaded: undefined,
};

export const masa: {
  body: undefined | PIXI.Texture;
  herraHakku: undefined | PIXI.Texture;
  leftArm: undefined | PIXI.Texture;
  rightArm: undefined | PIXI.Texture;
} = {
  body: undefined,
  herraHakku: undefined,
  leftArm: undefined,
  rightArm: undefined,
};

export const terrain: {
  dug: undefined | PIXI.Texture;
  undug: undefined | PIXI.Texture;
} = {
  dug: undefined,
  undug: undefined,
};
