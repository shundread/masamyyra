// External libraries
import * as PIXI from "pixi.js";
import * as TWEEN from "tween.js";

// Game libraries
import * as DisplayInfo from "./displayInfo";
import * as Duration from "./duration";
import * as FontNames from "./fontNames";
import * as Spritesheet from "./spritesheet";
import { waitTween } from "./waiter";

// Tints / Colors
const SkyTint: number = 0x00ebff;
const TextTint: number = 0xfeb874;

// Text style
const TextStyle: PIXI.extras.BitmapTextStyle = {
  align: "center",
  font: FontNames.komikagl,
  tint: TextTint,
};

const CloudQuantity: number = 6;

class SurfaceScene {
  public stage: PIXI.Container = new PIXI.Container();

  public ySpeed: number = 0;

  private graphics: PIXI.Graphics = new PIXI.Graphics();
  private clouds: PIXI.Sprite[] = [];
  private cloudSpeeds: number[] = [];

  private cloudsForeground: PIXI.Container = new PIXI.Container();
  private text: PIXI.extras.BitmapText = new PIXI.extras.BitmapText("", TextStyle);
  private cloudsBackground: PIXI.Container = new PIXI.Container();
  private house?: PIXI.Sprite;

  private startGameCallback: () => void;

  constructor(startGameCallback: () => void) {
    this.startGameCallback = startGameCallback;

    this.stage.addChild(this.graphics);
    this.stage.addChild(this.cloudsForeground);
    this.stage.addChild(this.text);
    this.stage.addChild(this.cloudsBackground);

    this.text.anchor = new PIXI.Point(0.5, 0.5);

    // Render complex layers
    this.makeBlueSky();
    this.generateClouds();

    // Set callback
    this.graphics.on("pointerdown", () => this.startGameCallback());
  }

  public async showText(text: string, lingerFactor: number = 1, scale: number = 1) {
    this.text.rotation = 0;
    this.text.scale.set(1, 1);
    this.text.text = text;
    this.text.position.set(0, DisplayInfo.Height);
    await waitTween(new TWEEN.Tween(this.text.position)
      .to({ y: 0 }, Duration.TextArrive)
      .easing(TWEEN.Easing.Quadratic.Out));
    new TWEEN.Tween(this.text)
      .to({ rotation: 0.5 * (Math.random() - 0.5)}, Duration.TextBump * lingerFactor)
      .easing(TWEEN.Easing.Elastic.Out)
      .start();
    await waitTween(new TWEEN.Tween(this.text.scale)
      .to({ x: scale, y: scale }, Duration.TextBump * lingerFactor)
      .easing(TWEEN.Easing.Elastic.Out));
    await waitTween(new TWEEN.Tween(this.text.position)
      .to({ y: -DisplayInfo.Height }, Duration.TextLeave)
      .easing(TWEEN.Easing.Quadratic.In));
  }

  public update(miliseconds: number) {
    for (let i = 0; i < this.clouds.length; i++) {
      const cloud: PIXI.Sprite = this.clouds[i];
      const xSpeed: number = this.cloudSpeeds[i];
      cloud.position.x += xSpeed;
      cloud.position.y += this.ySpeed * miliseconds;

      if (Math.abs(cloud.position.x) >= cloud.texture.width + DisplayInfo.Width) {
        this.resetCloud(i);
      }
      if (cloud.position.y <= -0.5 * (DisplayInfo.Height + cloud.texture.height)) {
        cloud.position.y = (1 + Math.random()) * DisplayInfo.Height;
        cloud.position.x = (Math.random() - 0.5) * DisplayInfo.Width;
      }
    }
  }

  public async addHouse(timeToShow: number) {
    this.house = new PIXI.Sprite(Spritesheet.intro.houseScene);
    this.house.anchor.set(0.5, 0.5);

    const y0: number = timeToShow * -this.ySpeed + DisplayInfo.Height;
    const y1: number = (DisplayInfo.Height - this.house.height) * 0.5;
    this.house.position.set(0, y0);

    this.stage.addChild(this.house);

    new TWEEN.Tween(this.house.position)
      .to({ y: y1 }, timeToShow)
      .easing(TWEEN.Easing.Quadratic.Out)
      .start();

    await waitTween(new TWEEN.Tween(this)
      .to({ ySpeed: 0 }, timeToShow)
      .easing(TWEEN.Easing.Quadratic.Out));
  }

  public setInteractive(value: boolean) {
    this.graphics.interactive = value;
  }

  private makeBlueSky() {
    this.graphics.beginFill(SkyTint);
    this.graphics.drawRect(0, 0, DisplayInfo.Width, DisplayInfo.Height);
    this.graphics.endFill();
    this.graphics.pivot.set(0.5 * DisplayInfo.Width, 0.5 * DisplayInfo.Height);
    // TODO add mask
  }

  private generateClouds() {
    while (this.clouds.length < CloudQuantity) {
      // Roll on cloud position
      const x: number = (Math.random() - 0.5) * DisplayInfo.Width;
      const y: number = (Math.random() - 0.5) * DisplayInfo.Height;

      // Spawn sprite and set initial location
      const sprite = new PIXI.Sprite(Spritesheet.intro.cloud);
      sprite.anchor.set(0.5, 0.5);
      sprite.position.set(x, y);

      // Roll on cloud scale
      const scale: number = (Math.random() + 0.5) * 0.5;
      if (Math.random() > 0) {
        sprite.scale.set(scale, scale);
      } else {
        sprite.scale.set(-scale, scale);
      }

      if (this.clouds.length < CloudQuantity / 2) {
        this.cloudsBackground.addChild(sprite);
      } else {
        this.cloudsForeground.addChild(sprite);
      }
      this.clouds.push(sprite);

      // Add a speed
      this.cloudSpeeds.push(this.generateCloudSpeed());

    }
  }

  private generateCloudSpeed(): number {
    const speed: number = 0.7 + Math.random();
    if (Math.random() < 0.5) {
      return -speed;
    }
    return speed;
  }

  private resetCloud(index: number) {
    const cloud: PIXI.Sprite = this.clouds[index];

    const speed: number = this.generateCloudSpeed();
    this.cloudSpeeds[index] = speed;

    if (speed > 0) {
      cloud.position.x = -DisplayInfo.Width;
    } else {
      cloud.position.x = DisplayInfo.Width;
    }

    cloud.position.y = (Math.random() - 0.5) * DisplayInfo.Height;
  }
}

export {
  SurfaceScene,
};
