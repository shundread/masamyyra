// External libraries
import * as PIXI from "pixi.js";

// Game libraries
import * as Spritesheet from "./spritesheet";

interface ISpritesheet {
  [name: string]: PIXI.Texture;
}

interface ISpritesheetMap {
  [name: string]: ISpritesheet;
}

const initialized: { [name: string]: boolean } = {};

function initializeSpritesheet(sheetName: string) {
  console.log(`parseTextures(${sheetName})`);
  const spritesheetMap = Spritesheet as ISpritesheetMap;
  const spritesheet: ISpritesheet = spritesheetMap[sheetName];
  if (spritesheet === undefined) {
    throw new Error(`SpriteSheet ${sheetName} not defined`);
  }

  if (initialized[sheetName]) {
    throw new Error(`Attempted to initialize ${sheetName} twice`);
  }

  const resource: PIXI.loaders.Resource = PIXI.loader.resources[sheetName];
  if (!resource) {
    throw new Error(`Could not find resource ${sheetName}`);
  }

  if (!resource.textures) {
    console.error(`No textures found in resource ${sheetName}`);
    console.error(resource);
    throw new Error(`No textures found in resource ${sheetName}`);
  }

  const textures: PIXI.loaders.TextureDictionary = resource.textures;
  for (const key of Object.keys(spritesheet)) {
    const simpleTextureKey: string = `${key}.png`;
    if (!textures[simpleTextureKey]) {
      throw new Error(`Spritesheet ${key} not found`);
    }
    console.log(`Found texture ${key}`);
    spritesheet[key] = textures[`${key}.png`];
  }

  initialized[sheetName] = true;
}

function initializeRemainingSpritesheets() {
  for (const key of Object.keys(Spritesheet)) {
    if (initialized[key]) {
      continue;
    }
    initializeSpritesheet(key);
  }
}

function loadRemainingSpritesheets() {
  for (const key of Object.keys(Spritesheet)) {
    if (Object.keys(PIXI.loader.resources).includes(key)) {
      continue;
    }
    console.log(`Auto-discovered resource ${key}`);
    PIXI.loader.add(key, `./graphics/${key}.json`);
  }
}

export {
  initializeSpritesheet,
  initializeRemainingSpritesheets,
  loadRemainingSpritesheets,
};
