// External libraries
import * as PIXI from "pixi.js";
import "pixi-sound"; // tslint:disable-line:ordered-imports

let intro: PIXI.sound.Sound;
let themeStart: PIXI.sound.Sound;
let themeLoop: PIXI.sound.Sound;
let diamond: PIXI.sound.Sound;

function loadSounds() {
  PIXI.loader.add("./sounds/introSong.{ogg,m4a}");
  PIXI.loader.add("./sounds/themeStart.{ogg,m4a}");
  PIXI.loader.add("./sounds/themeLoop.{ogg,m4a}");
  PIXI.loader.add("./sounds/diamond.wav");
}

async function initializeSound(url: string): Promise<PIXI.sound.Sound> {
  const promise: Promise<PIXI.sound.Sound> = new Promise((resolve: (sound: PIXI.sound.Sound) => void) => {
    // tslint:disable:object-literal-sort-keys
    PIXI.sound.Sound.from({
      url,
      autoPlay: false,
      preload: true,
      loaded: (error: Error, sound?: PIXI.sound.Sound, instance?: PIXI.sound.IMediaInstance) => {
        if (!sound && !instance)  {
          throw new Error(`Unable to load sound "${url}": ${error.message}`);
        }
        if (!sound) {
          throw new Error(`Expected a sound, not an instance for "${url}"`);
        }
        console.log(`Initialized audio "${url}" successfully`);
        resolve(sound);
      },
    });
    // tslint:enable:object-literal-sort-keys
  });
  return await promise;
}

async function initializeSounds() {
  intro = await initializeSound("./sounds/introSong.{ogg,m4a}");
  themeStart = await initializeSound("./sounds/themeStart.{ogg,m4a}");
  themeLoop = await initializeSound("./sounds/themeLoop.{ogg,m4a}");
  diamond = await initializeSound("./sounds/diamond.wav");
}

function startMusic() {
  intro.play(() => {
    themeStart.play(() => {
      themeLoop.play({ loop: true });
    });
  });
}

export {
  loadSounds,
  initializeSounds,
  startMusic,
  diamond,
};
