// Main duration classes
export const Tiny: number = 50;
export const Shorter: number = 100;
export const Short: number = 150;
export const NotSoShort: number = 200;
export const Medium: number = 500;
export const Long: number = 1000;
export const Linger: number = 1500;
export const Huge: number = 3000;

// Intro durations
export const FadeIn: number = Medium;
export const StartDrop: number = Linger;
export const MasaReveal: number = Long;

// Start game durations
export const HalfFlip: number = Long;

// End game durations
export const SlowDown: number = Long;
export const GoToSurface: number = Huge;
export const DumpDiamondInterval: number = Tiny;
export const DumpDiamond: number = Medium;

// Gameplay durations
export const MoveToColumn: number = Shorter;
export const CollectDiamond: number = Long;
export const AnimateScore: number = NotSoShort;

// Masa character animation durations
export const MasaWave: number = Short;
export const MasaDive: number = Short;
export const MasaRest: number = Short;

// Surface scene durations
// NOTE: those are set to specific independent numbers as they happen to be
//       timed to match the introduction song
export const TextArrive: number = 250;
export const TextLeave: number = 250;
export const TextBump: number = 1500;
export const HouseArrive: number = 1500;
