// External libraries
import * as PIXI from "pixi.js";
import * as TWEEN from "tween.js";

// Game libraries
import * as Duration from "./duration";
import * as Spritesheet from "./spritesheet";

class Masa {
  public body: PIXI.Sprite;
  private leftArm: PIXI.Sprite;
  private rightArm: PIXI.Sprite;
  private herraHakku: PIXI.Sprite;

  private armTweens: TWEEN.Tween[] = [];

  constructor() {
    // Spawn the sprites
    this.body = new PIXI.Sprite(Spritesheet.masa.body);
    this.leftArm = new PIXI.Sprite(Spritesheet.masa.leftArm);
    this.rightArm = new PIXI.Sprite(Spritesheet.masa.rightArm);
    this.herraHakku = new PIXI.Sprite(Spritesheet.masa.herraHakku);

    // Shifts
    const dx: number = 0.5 * this.body.texture.width;
    const dy: number = 0.5 * this.body.texture.height;

    // Set their anchors and positions
    this.leftArm.anchor.set(1, 0);
    this.rightArm.anchor.set(0, 0);
    this.herraHakku.anchor.set(0.2, 1);
    this.body.anchor.set(0.5, 0.5);
    this.leftArm.position.set(40 - dx, 190 - dy);
    this.rightArm.position.set(140 - dx, 160 - dy);
    this.herraHakku.position.set(80, 70);
    this.body.addChild(this.leftArm);
    this.body.addChild(this.rightArm);
    this.rightArm.addChild(this.herraHakku);
  }

  public setArmWaving() {
    this.stopArmTweens();

    this.leftArm.rotation = 0.5 * Math.PI;
    this.rightArm.rotation = -0.4 * Math.PI;

    this.armTweens.push(new TWEEN.Tween(this.leftArm)
      .to({ rotation: 0.8 * Math.PI }, Duration.MasaWave)
      .yoyo(true)
      .repeat(Infinity));
    this.armTweens.push(new TWEEN.Tween(this.rightArm)
    .to({ rotation: -0.7 * Math.PI }, Duration.MasaWave)
    .yoyo(true)
    .repeat(Infinity));

    this.startArmTweens();
  }

  public setArmDiving() {
    this.stopArmTweens();

    this.armTweens.push(new TWEEN.Tween(this.leftArm)
      .to({ rotation: 0.5 * Math.PI }, Duration.MasaDive));
    this.armTweens.push(new TWEEN.Tween(this.rightArm)
    .to({ rotation: -0.4 * Math.PI }, Duration.MasaDive));

    this.startArmTweens();
  }

  public setArmResting() {
    this.stopArmTweens();

    this.armTweens.push(new TWEEN.Tween(this.leftArm)
      .to({ rotation: 0.0 }, Duration.MasaRest));
    this.armTweens.push(new TWEEN.Tween(this.rightArm)
    .to({ rotation: 0.0 }, Duration.MasaRest));

    this.startArmTweens();
  }

  private stopArmTweens() {
    for (const tween of this.armTweens) {
      tween.stop();
    }
    this.armTweens = [];
  }

  private startArmTweens() {
    for (const tween of this.armTweens) {
      tween.start();
    }
  }
}

export {
  Masa,
};
