// External libraries
import MobileDetect from "mobile-detect";

const Width: number = 720;
const Height: number = 1280;

interface ISize {
  width: number;
  height: number;
}

interface IDisplayInfo {
  mobile: boolean;
  renderer: ISize;
  rotation: number;
  scale: number;
}

function getDisplayInfo(): IDisplayInfo {
  const renderer: ISize = {
    width: document.body.clientWidth,
    height: document.body.clientHeight,
  };

  const mobileDetect: MobileDetect = new MobileDetect(window.navigator.userAgent);
  const mobile: boolean = Boolean(mobileDetect.mobile());

  return {
    mobile,
    renderer,
    rotation: getRendererRotation(renderer, mobile),
    scale: getRendererScale(renderer, mobile),
  };
}

function getRendererRotation(renderer: ISize, mobile: boolean) {
  if ((mobile === true) && (renderer.width > renderer.height)) {
    return 0.5 * Math.PI;
  }
  return 0;
}

function getRendererScale(renderer: ISize, mobile: boolean) {
  let width: number = renderer.width;
  let height: number = renderer.height;
  if (mobile) {
    width = Math.min(renderer.width, renderer.height);
    height = Math.max(renderer.width, renderer.height);
  }
  const wScale: number = width / Width;
  const hScale: number = height / Height;
  return Math.min(wScale, hScale);
}

export {
  Width,
  Height,
  ISize,
  IDisplayInfo,
  getDisplayInfo,
}
