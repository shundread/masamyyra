const path = require('path');

module.exports = {
  entry: './src/main.ts',
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [ "ts-loader" ],
      },
    ],
  },
  output: {
    path: path.resolve(__dirname, "build"),
    filename: 'bundle.js',
  },
  resolve: {
   extensions: [ '.ts', '.js' ],
  },
  externals: {
    "pixi.js": "PIXI",
    "pixi-sound": "PIXI.sound",
    "tween.js": "TWEEN",
    "mobile-detect": "MobileDetect",
  },
};
